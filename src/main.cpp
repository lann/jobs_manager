#include <libintl.h>
#include <locale.h>
#include "job_manager.h"
#include <gtkmm/main.h>
#ifdef HAVE_CONFIG_H
  #include "config.h"
#endif
#define _(String) gettext(String)

int main(int argc, char *argv[])
{
     setlocale (LC_ALL, "");
#ifdef HAVE_CONFIG_H
     {
	  bindtextdomain (PACKAGE, LOCALEDIR);
	  bind_textdomain_codeset(PACKAGE, "UTF-8");
	  textdomain (PACKAGE);
     }
#else
     {
	  bindtextdomain("word_search","/usr/share/locale");
	  bind_textdomain_codeset("word_search", "UTF-8");
	  textdomain("word_search");
     }
#endif
     auto app=Gtk::Application::create(argc, argv);
     Job_Manager *pwindow=nullptr;
     pwindow=new Job_Manager;
     if (pwindow)
     {
	  return app->run(*pwindow);
     }
     else
     {
	  return 1;
     }
     return 0;
}
